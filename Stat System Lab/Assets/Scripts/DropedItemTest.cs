﻿using UnityEngine;
using UnityEditor;

public class DropedItemTest: MonoBehaviour{
    [SerializeField] ItemDefinition item;
    [SerializeField] Sprite mask;
    [SerializeField] Camera playerCam;
    [SerializeField] float pickupTime = 2.0f; 

    public void DroppedItemTest(ItemDefinition item_) {
        item = item_;
        try {
            mask = item.ItemSprite;
        } catch(MissingReferenceException) {
            Debug.Log("No sprite found for item; default selected.");
            mask = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Sprites/Default.png", typeof(Sprite));
        }
    }

    public void DroppedItemTest(ItemDefinition item_, Sprite mask_) { 
        item = item_;
        mask = mask_;
    }//overload incase I ever want to use a different sprite than the item's sprite

    private void Awake() {
        gameObject.GetComponent<SpriteRenderer>().sprite = mask;
        playerCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    private void Update() {
        transform.LookAt(playerCam.transform);

        if(pickupTime > 0) {
            pickupTime -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(pickupTime <= 0) {
            if(other.CompareTag("Player")) {
                if(other.GetComponent<CharacterInput>().inventory.GetComponent<InventoryController>().PickupItem(item)) {
                    GameObject.Destroy(gameObject);
                }
            }
        }
    }

}
