﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class InventoryController : MonoBehaviour {
    [SerializeField] Vector2 inputM;
    [SerializeField] float inputBuffer = 0.25f;
    [SerializeField] float counter = 0.0f;
    [SerializeField] bool buffer = false;
    [SerializeField] float deadZone = 0.4f;
    [SerializeField] Text textBox, strMod, dexMod, conMod, intMod, wisMod, chrMod;
    [SerializeField] GameObject instructions, blankItem;
    [SerializeField] CharacterController player;

    [SerializeField] int[] selectionPos = new int[2] { 0, 0 };
    [SerializeField] int[] prevSelection = new int[2] { 0, 0 };
    [SerializeField] int[] selected = new int[2] { -1,-1};

    [SerializeField] bool[,] lockedSlot = new bool[19, 2];
    [SerializeField] GameObject[,] graphicalInventory = new GameObject[19, 2];
    [SerializeField] ItemDefinition[,] inventory = new ItemDefinition[19, 2];
    //below is a representation of the graphical inventory based on above split array
    //      [15,0][15,1]    [00,0][00,1][01,0][01,1][02,0][02,1]
    //[16,0][16,1][17,0]    [03,0][03,1][04,0][04,1][05,0][05,1]
    //[fill][fill][fill]    [06,0][06,1][07,0][07,1][08,0][08,1]
    //[17,1][18,0][18,1]    [09,0][09,1][10,0][10,1][11,0][11,1]
    //      [fill]          [12,0][12,1][13,0][13,1][14,0][14,1]

    private void Awake() {
        instructions.SetActive(false);
        GameObject[] temp = GameObject.FindGameObjectsWithTag("slot");
        for(int i = 0; i < temp.Length; i++) {
            graphicalInventory[i/2, i % 2] = temp[i];
        }

        AddItems();
    }

    private void AddItems() { //for default inventory/testing purposes
        inventory[0, 1] = (ItemDefinition)AssetDatabase.LoadAssetAtPath("Assets/Items/HelmOfSpeed.asset", typeof(ItemDefinition));
        graphicalInventory[0, 1].GetComponent<RawImage>().texture = inventory[0, 1].ItemSprite.texture;

        inventory[1, 1] = (ItemDefinition)AssetDatabase.LoadAssetAtPath("Assets/Items/StuddedLeather.asset", typeof(ItemDefinition));
        graphicalInventory[1, 1].GetComponent<RawImage>().texture = inventory[1, 1].ItemSprite.texture;

        inventory[3, 1] = (ItemDefinition)AssetDatabase.LoadAssetAtPath("Assets/Items/NecklaceOfSpeed.asset", typeof(ItemDefinition));
        graphicalInventory[3, 1].GetComponent<RawImage>().texture = inventory[3, 1].ItemSprite.texture;
    }

    private void Update() {
        if(!buffer) {
            buffer = true;
            if(inputM[0] > deadZone) {
                if(selectionPos[1] == 0) {
                    if(selectionPos[0] == 17) {
                        selectionPos[0] = 3;
                    } else {
                        selectionPos[1] = 1;
                    }
                }
                else if(selectionPos[1] == 1) {
                    switch(selectionPos[0]) {
                        case 2:
                        case 5:
                        case 8:
                        case 11:
                        case 14:
                            break;
                        case 15:
                            selectionPos[1] = 0;
                            selectionPos[0] = 0;
                            break;
                        case 18:
                            selectionPos[1] = 0;
                            selectionPos[0] = 9;
                            break;
                        default:
                            selectionPos[0] += 1;
                            selectionPos[1] = 0;
                            break;
                    }
                }
            }
            else if(inputM[0] < -deadZone) {
                if(selectionPos[1] == 1) {
                    if(selectionPos[0] != 17)
                        selectionPos[1] = 0;
                } else if(selectionPos[1] == 0) {
                    switch(selectionPos[0]) {
                        case 0:
                            selectionPos[0] = 15;
                            selectionPos[1] = 1;
                            break;
                        case 3:
                        case 6:
                            selectionPos[0] = 17;
                            break;
                        case 9:
                        case 12:
                            selectionPos[0] = 18;
                            selectionPos[1] = 1;
                            break;
                        case 15:
                        case 16:
                            break;
                        default:
                            selectionPos[1] = 1;
                            selectionPos[0] -= 1;
                            break;
                    }
                }
            }
            else if(inputM[1] > deadZone) {
                switch(selectionPos[0]) {
                    case 0:
                    case 1:
                    case 2:
                    case 15:
                        break;
                    case 16:
                        if(selectionPos[1] == 1) {
                            selectionPos[0] = 15;
                            selectionPos[1] = 0;
                        }
                        break;
                    case 17:
                    case 18:
                        if(selectionPos[1] == 0) {
                            selectionPos[0] -= 2;
                            selectionPos[1] = 1;
                        }
                        else if(selectionPos[1] == 1) {
                            selectionPos[0] -= 1;
                            selectionPos[1] = 0;
                        }
                        break;
                    default:
                        selectionPos[0] -= 3;
                        break;
                }
            }
            else if(inputM[1] < -deadZone) {
                switch(selectionPos[0]) {
                    case 12:
                    case 13:
                    case 14:
                    case 18:
                        break;
                    case 15:
                    case 16:
                        if(selectionPos[1] == 0) {
                            selectionPos[0] += 1;
                            selectionPos[1] = 1;
                        } else if(selectionPos[1] == 1) {
                            selectionPos[0] += 2;
                            selectionPos[1] = 0;
                        }
                        break;
                    case 17:
                        if(selectionPos[1] == 0) {
                            selectionPos[0] = 18;
                            selectionPos[1] = 1;
                        }
                        break;
                    default:
                        selectionPos[0] += 3;
                        break;
                }
            }
            else{ buffer = false;}
            graphicalInventory[prevSelection[0], prevSelection[1]].GetComponent<RawImage>().color = Color.white;
            graphicalInventory[selectionPos[0], selectionPos[1]].GetComponent<RawImage>().color = Color.yellow;
            if(lockedSlot[prevSelection[0], prevSelection[1]])
                graphicalInventory[prevSelection[0], prevSelection[1]].GetComponent<RawImage>().color = Color.blue;
            prevSelection[0] = selectionPos[0];
            prevSelection[1] = selectionPos[1];
        } else {
            counter += Time.deltaTime;
            if(counter >= inputBuffer) {
                counter = 0;
                buffer = false;
            }
        }
    }

    public void GiveInfo(Vector2 input_) {
        inputM = input_;
    }

    public void Select() {
        if(selected[0] > -1) { //if something is selected 
            if(inventory[selectionPos[0], selectionPos[1]] == null) { //if the hovered slot is empty
                if(selectionPos[0] < 15) { // if the hovered slot is not a character item slot
                    if (inventory[selected[0], selected[1]].Equiped){ //if it's currently equiped
                        EquipItem();
                    }else{ //if it's not currently equiped
                        MoveItem();
                    }
                } else {
                    switch(inventory[selected[0], selected[1]].Slot) {
                        case "Head":
                            if(selectionPos[0] == 15 && selectionPos[1] == 0)
                                EquipItem();
                            break;
                        case "Neck":
                            if(selectionPos[0] == 15 && selectionPos[1] == 1)
                                EquipItem();
                            break;
                        case "On_Hand":
                            if(selectionPos[0] == 16 && selectionPos[1] == 0)
                                EquipItem();
                            break;
                        case "Torso":
                            if(selectionPos[0] == 16 && selectionPos[1] == 1)
                                EquipItem();
                            break;
                        case "Off_Hand":
                            if(selectionPos[0] == 17 && selectionPos[1] == 0)
                                EquipItem();
                            break;
                        case "RingLeft":
                            if(selectionPos[0] == 17 && selectionPos[1] == 1)
                                EquipItem();
                            break;
                        case "Legs":
                            if(selectionPos[0] == 18 && selectionPos[1] == 0)
                                EquipItem();
                            break;
                        case "RingRight":
                            if(selectionPos[0] == 18 && selectionPos[1] == 1)
                                EquipItem();
                            break;
                    }
                }
                if(inventory[selected[0],selected[1]] == null) {
                    selected[0] = -1;
                    selected[1] = -1;
                    instructions.SetActive(false);
                }
            } else {
                //to-do; add the ability to swap selected item with another item in inventory
            }

        } else {
            if(inventory[selectionPos[0], selectionPos[1]] != null) {
                lockedSlot[selectionPos[0], selectionPos[1]] = true;
                selected[0] = selectionPos[0];
                selected[1] = selectionPos[1];
                textBox.text = inventory[selectionPos[0], selectionPos[1]].ItemName;
                instructions.SetActive(true);
            }
           
        }
    }

    private void EquipItem() {
        player.PlayerStats.ToggleItemEquip(inventory[selected[0], selected[1]]);
        MoveItem();
        UpdateStats();
    }

    private void MoveItem()
    {
        inventory[selectionPos[0], selectionPos[1]] = inventory[selected[0], selected[1]];
        try{
            graphicalInventory[selectionPos[0], selectionPos[1]].GetComponent<RawImage>().texture = inventory[selectionPos[0], selectionPos[1]].ItemSprite.texture;
        }catch (MissingReferenceException){
            graphicalInventory[selectionPos[0], selectionPos[1]].GetComponent<RawImage>().texture = ((Sprite)AssetDatabase.LoadAssetAtPath("Assets/Sprites/Default.png", typeof(Sprite))).texture;
        }

        inventory[selected[0], selected[1]] = null;
        graphicalInventory[selected[0], selected[1]].GetComponent<RawImage>().texture = null;
        lockedSlot[selected[0], selected[1]] = false;
        graphicalInventory[selected[0], selected[1]].GetComponent<RawImage>().color = Color.white;
        textBox.GetComponent<Text>().text = "";
    }

    public void RemoveSelected() {
        if(selected[0] > -1) {
            if(selected[0] > 14) {
                player.PlayerStats.ToggleItemEquip(inventory[selected[0], selected[1]]);
                UpdateStats();
            }
            lockedSlot[selected[0], selected[1]] = false;
            graphicalInventory[selected[0], selected[1]].GetComponent<RawImage>().color = Color.white;
            graphicalInventory[selected[0], selected[1]].GetComponent<RawImage>().texture = null;
            GameObject droppedItem = GameObject.Instantiate(blankItem, GameObject.FindGameObjectWithTag("Player").transform.position, new Quaternion());
            droppedItem.GetComponent<DroppedItem>().GiveItem(inventory[selected[0],selected[1]]);
            inventory[selected[0], selected[1]] = null;
            selected[0] = -1;
            selected[1] = -1;
            textBox.GetComponent<Text>().text = "";
            instructions.SetActive(false);
        }
    }

    public void UpdateStats() {
        strMod.text = player.PlayerStats.GetStat("Strength").Number.ToString();
        dexMod.text = player.PlayerStats.GetStat("Dexterity").Number.ToString();
        conMod.text = player.PlayerStats.GetStat("Constitution").Number.ToString();
        intMod.text = player.PlayerStats.GetStat("Intelligence").Number.ToString();
        wisMod.text = player.PlayerStats.GetStat("Wisdom").Number.ToString();
        chrMod.text = player.PlayerStats.GetStat("Charisma").Number.ToString();
    }

    public bool PickupItem(ItemDefinition item_) {
        for(int i = 0; i < 15; i++) {
            for(int j = 0; j < 2; j++) {
                if(inventory[i,j] == null) {
                    inventory[i, j] = item_;
                    graphicalInventory[i, j].GetComponent<RawImage>().texture = inventory[i, j].ItemSprite.texture;
                    return true;
                }
            }
        }
        return false;
    }
}
