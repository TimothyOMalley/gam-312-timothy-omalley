﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour{
    [SerializeField] float walk = 3.0f;
    [SerializeField] float speedMod = 3.0f;
    [SerializeField] float regRotationSpeed = 100f;
    [SerializeField] float rotationSpeed = 100f;
    [SerializeField] float windUpFrames = 50f;
    [SerializeField] float maxHealth = 100f;
    [SerializeField] float currentHealth = 100f;
    [SerializeField] float percentileMaxHealthUpdate = 1f; //100% health recovered on health update tick 
    [SerializeField] float healthUpdateTick = 5f;
    [SerializeField] float currentTick = 0;
    [SerializeField] RawImage currentHealthImage;
    [SerializeField] Text health;
    private float doubleTime, doubleTimeRotation;
    public bool run = false;

    StatSystem playerStats = new StatSystem();

    [SerializeField] Vector2 inputM; //input-Movement
    private Rigidbody character;

    void Awake(){
        character = GetComponent<Rigidbody>();

        doubleTime = 2 * speedMod;
        doubleTimeRotation = 2 * rotationSpeed;
    }

    private void Update(){
        transform.Rotate(transform.up * inputM.x * rotationSpeed * Time.fixedDeltaTime);
        if (run){ //on run
            if(speedMod < doubleTime){ //increase the speed mod/rotation speed by a fraction of the double time
                speedMod += ((doubleTime - walk) / windUpFrames);
                rotationSpeed += ((doubleTimeRotation - regRotationSpeed) / windUpFrames);
            }
        }
        else{
            if(speedMod > walk) { //otherwiae DECREASE the speed mod/rotation speed by a fraction of double time
                speedMod -= ((doubleTime - walk) / windUpFrames);
                rotationSpeed -= ((doubleTimeRotation - regRotationSpeed) / windUpFrames);
            }
        }
        if(currentHealth < maxHealth){
            if(currentTick >= healthUpdateTick){
                currentHealth += maxHealth * percentileMaxHealthUpdate; //heal for 2.5% of max health 
                currentTick = 0f;
            }
            currentTick += Time.deltaTime;
        }
        if (currentHealth > maxHealth) { currentHealth = maxHealth; }
    }

    void FixedUpdate(){
        character.MovePosition(character.position + (transform.forward * inputM.y) * speedMod * Time.fixedDeltaTime * (1+(playerStats.GetStat(1).Number/10)));
        maxHealth = 100 * (1 + (playerStats.GetStat("Constitution").Number / 10));
        UpdateUI();
    }

    void UpdateUI(){
        health.text = "Health: " + currentHealth + "/" + maxHealth;
        currentHealthImage.transform.localScale = new Vector3((currentHealth / maxHealth),1,1);
    }

    public Vector2 InputM
    {
        get { return inputM; }
        set { inputM = value; }
    }
    public StatSystem PlayerStats{ get { return playerStats; } } //PlayerStats ( get; )
}
