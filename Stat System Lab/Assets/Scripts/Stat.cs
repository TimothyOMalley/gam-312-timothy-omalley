﻿using System.Collections;

public class Stat {

    string name;
    float initalStat;
    float currentStat;

    public Stat(string name_, float stat) {
        name = name_;
        initalStat = stat;
    }

    public string Name {
        get{ return name; }
    }

    public float Number {
        get { return currentStat; }
        set { currentStat = value; }
    }

    public void Reset() {
        currentStat = initalStat;
    }
}
