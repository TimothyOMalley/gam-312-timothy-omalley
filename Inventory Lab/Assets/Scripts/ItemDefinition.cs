﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "DefualtItemDefinition", menuName = "ScriptableObjects/Item", order = 1)]

public class ItemDefinition : ScriptableObject{
    [SerializeField] string itemName;
    [SerializeField] bool equiped = false;
    [SerializeField] Sprite itemSprite;
    //[SerializeField] GameObject itemObject;

    [SerializeField] enum EquipSlot {Head, Neck, Torso, Legs, RingRight, RingLeft, On_Hand, Off_Hand}
    [SerializeField] EquipSlot slot;
    [SerializeField] byte id;

    [SerializeField] int Strength,Dexterity,Constitution;
    //[SerializeField] float Inteligence;
    //[SerializeField] float Wisdom;
    //[SerializeField] float Charisma;

    private void OnEnable() {
        if(itemSprite == null) {
            itemSprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Sprites/Default.png", typeof(Sprite));
        }
    }

    public string getEquipSlot() {
        return slot.ToString();
    }

    public void SetEquiped(bool eq) {
        equiped = eq;
    }

    public bool GetEquiped() {
        return equiped;
    }

    public Sprite GetSprite() {
        return itemSprite;
    }

    public string GetName() {
        return itemName;
    }

    public int[] GetStats() {
        return new int[] { Constitution, Dexterity, Strength };
    }

    public byte GetID() {
        return id;
    }
}
