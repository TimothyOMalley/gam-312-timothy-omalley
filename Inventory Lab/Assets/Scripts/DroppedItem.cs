﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItem: MonoBehaviour{
    Camera playerCam;
    [SerializeField] ItemDefinition item;
    [SerializeField] float pickupTime = 2.0f;
    [SerializeField] bool pickup = false;

    private void Awake() {
        gameObject.GetComponent<Rigidbody>().velocity = gameObject.transform.forward;
        playerCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        if(item != null) {
            GiveItem(item);
        }
    }

    public void GiveItem(ItemDefinition item_) {
        item = item_;
        try {
            gameObject.GetComponent<SpriteRenderer>().sprite = item.GetSprite();
        } catch(MissingReferenceException) { }
    }

    void Update(){
        transform.LookAt(playerCam.transform);
        if(pickupTime >= 0) {
            pickupTime -= Time.deltaTime;
        } else { pickup = true; }
    }

    private void OnTriggerEnter(Collider other) {
        if(pickup) {
            if(other.CompareTag("Player")) {
                if(other.GetComponent<CharacterInput>().inventory.GetComponent<InventoryController>().PickupItemByID(item.GetID())) {
                    GameObject.Destroy(gameObject);
                }
            }
        }
    }

}
