﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInput : MonoBehaviour{
    [SerializeField] CharacterController character;
    public GameObject inventory;
    [SerializeField] InventoryController inventory_;

    public enum ControlScheme {player, inventory}
    ControlScheme scheme = ControlScheme.player;

    private void Awake(){
        character = gameObject.GetComponent<CharacterController>();
        inventory = GameObject.FindGameObjectWithTag("Inventory");
        inventory_ = inventory.GetComponent<InventoryController>();

        inventory.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if(movementInput.magnitude >= 1.0f)
            movementInput = movementInput.normalized;

        switch(scheme) {
            case ControlScheme.player:
                character.GiveInfo(movementInput);

                if(Input.GetButtonDown("Jump"))
                    character.run = true;
                else if(Input.GetButtonUp("Jump"))
                    character.run = false;

                if(Input.GetButtonDown("Inventory")) {
                    scheme = ControlScheme.inventory;
                    character.GiveInfo(new Vector2(0, 0));
                    inventory.SetActive(true);
                }
                break;
            case ControlScheme.inventory:
                inventory_.GiveInfo(movementInput);

                if(Input.GetButtonDown("Jump")) {
                    inventory_.Select();
                }

                if(Input.GetButtonDown("Inventory")) {
                    scheme = ControlScheme.player;
                    inventory.SetActive(false);
                }

                if(Input.GetButtonDown("Y")){
                    inventory_.RemoveSelected();
                }
                break;
        }
    }


}
