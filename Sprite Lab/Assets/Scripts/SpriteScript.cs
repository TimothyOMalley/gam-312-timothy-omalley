﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteScript : MonoBehaviour {
    [SerializeField] Camera playerCam;

    void Update() {
        transform.LookAt(playerCam.transform);
    }
}
