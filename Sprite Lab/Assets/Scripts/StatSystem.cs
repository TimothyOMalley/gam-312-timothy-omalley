﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices.ComTypes;
using UnityEditor.PackageManager;
using UnityEngine;

public class StatSystem{
    delegate void StatChange(StatSystem args);
    StatChange statChange;
    public enum StatChangeType { Set, Change, Reset };

    Stat[] playerStats;

    public StatSystem(){ //default stats
        playerStats = new Stat[6] {
            new Stat("Strength",0),
            new Stat("Dexterity",0),
            new Stat("Constitution",0),
            new Stat("Intelligence",0),
            new Stat("Wisdom",0),
            new Stat("Charisma",0)
        };
    }

    public StatSystem(Stat[] stats) {
        playerStats = stats;
    }

    public Stat[] PlayerStats{
        get { return playerStats; }
    }

    public StatSystem(int[] sameStatsDifferentValues) {
        playerStats = new Stat[6] {
            new Stat("Strength", sameStatsDifferentValues[0]),
            new Stat("Dexterity", sameStatsDifferentValues[1]),
            new Stat("Constitution", sameStatsDifferentValues[2]),
            new Stat("Intelligence", sameStatsDifferentValues[3]),
            new Stat("Wisdom", sameStatsDifferentValues[4]),
            new Stat("Charisma", sameStatsDifferentValues[5])
        };
    }

    public Stat GetStat(object theStat) { //allows for an index OR a stat Name.
        if (theStat.GetType() == typeof(int)) {
            return playerStats[(int)theStat];
        }
        else if (theStat.GetType() == typeof(string)) {
            for (int statNum = 0; statNum <= playerStats.Length-1; statNum++){
                if (playerStats[statNum].Name.Equals(theStat)){
                    return (playerStats[statNum]);
                }
            }
        }
        Debug.LogError("Could not find stat of type "+theStat);
        return null;
    }

    public void ToggleItemEquip(ItemDefinition item){
        if (item.Equiped){
            statChange = MakeStatsNegative;
            statChange += ChangeXStats;
            statChange += MakeStatsNegative;
            item.Equiped = false;
        }else{
            statChange = ChangeXStats;
            item.Equiped = true;
        }
        statChange(item.ItemStats);
    }

    public void AlterStats(object stats,StatChangeType typeOfChange){

        switch (typeOfChange){ //calls if the stats need to be Set to x or Changed by X
            case StatChangeType.Set:
                statChange = SetXStats;
                break;
            case StatChangeType.Change:
                statChange = ChangeXStats;
                break;
            case StatChangeType.Reset:
                statChange = ResetXStats;
                break;
        }

        if(stats.GetType() == typeof(Stat[])){ //used object to allow for much more versitile method;
            statChange(new StatSystem((Stat[])stats));
        }else if(stats.GetType() == typeof(StatSystem)){
            statChange((StatSystem)stats);
        }else{
            Debug.LogError("Can't Set Stats with type: "+stats.GetType());
        }
    }

    void MakeStatsNegative(StatSystem args){
        foreach(Stat variable in args.playerStats){
            variable.Number *= -1;
        }
    }

    void ChangeXStats(StatSystem args){
        foreach(Stat variable in args.PlayerStats){
            foreach(Stat playerStat in playerStats){
                if (playerStat.Name.Equals(variable.Name)) {
                    playerStat.Number += variable.Number;
                }
            }
        }
    }

    void SetXStats(StatSystem args){
        foreach (Stat variable in args.PlayerStats){
            foreach (Stat playerStat in playerStats){
                if (playerStat.Name.Equals(variable.Name)){
                    playerStat.Number = variable.Number;
                }
            }
        }
    }
    
    void ResetXStats(StatSystem args){
        foreach (Stat variable in args.PlayerStats){
            foreach (Stat playerStat in playerStats){
                if (playerStat.Name.Equals(variable.Name)){
                    playerStat.Reset();
                }
            }
        }
    }
}
