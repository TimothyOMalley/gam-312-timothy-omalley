﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour{
    //The slot type determines what kind of items can be put in it
    public enum SlotType { 
        //armor slots
        Head, Neck, Shoulder, Chest, Wrist, Hand, Belt, Legs, Feet, Ring, Trinket, Weapon,
        //bag slots of only for bags, inventory is a generic inventory slot
        Bag, Inventory, 
        //Null is for a slot that is instantiated incorrectly so the game won't crash
        Null
    }

    SlotType slotCanHold;
    Item itemInSlot;

    public Item ItemInSlot{
        get { return itemInSlot; }
        set { ItemInSlot = value; }
    }

    public SlotType SlotCanHold {
        set { slotCanHold = value; }
    }

    //public void OnItemChange(){}
   
}
