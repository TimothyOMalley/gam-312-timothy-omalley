﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject{ //I want this to be a very barebones class that gets inherited from.
    public enum ItemType { Equipable, Bag, Consumable, Null }
    public ItemType typeOfItem;
    public string itemName;
    public string itemDescription;
    public Sprite itemSprite;
}
