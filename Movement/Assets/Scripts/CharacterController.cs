﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour{
    [SerializeField] float walk = 3.0f;
    [SerializeField] float speedMod = 3.0f;
    [SerializeField] float regRotationSpeed = 100f;
    [SerializeField] float rotationSpeed = 100f;
    [SerializeField] float windUpFrames = 50;
    private float doubleTime, doubleTimeRotation;
    public bool run = false;

    private Vector2 inputM; //input-Movement
    private Rigidbody character;

    void Awake(){
        character = GetComponent<Rigidbody>();

        doubleTime = 2 * speedMod;
        doubleTimeRotation = 2 * rotationSpeed;
    }

    private void Update(){
        transform.Rotate(transform.up * inputM.x * rotationSpeed * Time.fixedDeltaTime);
        if (run){ //on run
            if(speedMod < doubleTime){ //increase the speed mod/rotation speed by a fraction of the double time
                speedMod += ((doubleTime - walk) / windUpFrames);
                rotationSpeed += ((doubleTimeRotation - regRotationSpeed) / windUpFrames);
            }
        }
        else{
            if(speedMod > walk) { //otherwiae DECREASE the speed mod/rotation speed by a fraction of double time
                speedMod -= ((doubleTime - walk) / windUpFrames);
                rotationSpeed -= ((doubleTimeRotation - regRotationSpeed) / windUpFrames);
            }
        }
    }

    void FixedUpdate(){
        character.MovePosition(character.position + (transform.forward * inputM.y) * speedMod * Time.fixedDeltaTime);
    }

    public void GiveInfo(Vector2 _input){
        inputM = _input;
    }
}
