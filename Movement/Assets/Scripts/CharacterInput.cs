﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInput : MonoBehaviour{
    [SerializeField] CharacterController character;

    private void Awake(){
        character = gameObject.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update() {
        Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (movementInput.magnitude >= 1.0f)
            movementInput = movementInput.normalized;

        character.GiveInfo(movementInput);

        if (Input.GetButtonDown("Jump"))
            character.run = true;
        else if (Input.GetButtonUp("Jump"))
            character.run = false;
    }

}
