﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;
using UnityEngine.XR.WSA.Input;

[CreateAssetMenu(fileName = "DefualtItemDefinition", menuName = "ScriptableObjects/Item", order = 1)]

public class ItemDefinition : ScriptableObject{
    [SerializeField] string itemName;
    [SerializeField] bool equiped = false;
    [SerializeField] Sprite itemSprite;
    //[SerializeField] GameObject itemObject;

    [SerializeField] enum EquipSlot {Head, Neck, Torso, Legs, RingRight, RingLeft, On_Hand, Off_Hand}
    [SerializeField] EquipSlot slot;
    [SerializeField] float strength,dexterity,constitution,intelligence,wisdom,charisma; //for purposes of editing stats in-editor\
    StatSystem itemStats = new StatSystem();


    private void OnEnable() {
        equiped = false;
        if(itemSprite == null) { //if no sprite set; set to default sprite.
            itemSprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Sprites/Default.png", typeof(Sprite));
        }
    }

    private void OnValidate(){
        UpdateItemStatValues();
    }

    private void UpdateItemStatValues(){
        float[] statsAsFloats = new float[6] { strength, dexterity, constitution, intelligence, wisdom, charisma };
        StatSystem newStats = new StatSystem();
        for(int stat = 0; stat < 6; stat++){
            newStats.GetStat(stat).Number = statsAsFloats[stat];
        }
        itemStats.AlterStats(newStats, StatSystem.StatChangeType.Set);
    }


    public string Slot { get { return slot.ToString(); } } //returns enum slot as string

    public bool Equiped{ //Equiped( get; set; )
        get { return equiped; }
        set { equiped = value; }
    }

    public Sprite ItemSprite{ get { return itemSprite; } } //ItemSprite( get; )
    public string ItemName { get { return itemName; } } //ItemName( get; )
    public StatSystem ItemStats{ get { return itemStats; } } //ItemStats( get; )
}
