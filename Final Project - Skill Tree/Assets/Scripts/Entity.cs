﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public float maxHealth = 100f;
    public float currentHealth = 100f;

    public void ChangeHealth(float amount) {
        currentHealth += amount;
        if(currentHealth > maxHealth) {
            currentHealth = maxHealth;
        }
        if(currentHealth <= 0) {
            Destroy(gameObject);
        }
    }

}
