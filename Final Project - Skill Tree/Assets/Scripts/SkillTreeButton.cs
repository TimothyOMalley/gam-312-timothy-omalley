﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTreeButton : MonoBehaviour{
    [HideInInspector] //Hiding these after I've set them in the prefab, that way it's not cluttering up the window.
    [SerializeField] Text skillName;
    [HideInInspector]
    [SerializeField] Text skillCost;
    [HideInInspector]
    [SerializeField] CharacterController player;

    //Literally, this is the ONLY thing the person making the skill tree has to set.
    [SerializeField] AbilityDefinition ability;

    private void OnValidate() {  //When ability gets put into the prefab, it changed the name of and text on the button.
        if(ability != null) {
            gameObject.name = ability.SkillName+" Button";
            skillName.text = ability.SkillName;
            skillCost.text = "Cost: " + ability.SkillPointCost;
        } else {
            gameObject.name = "Skill Name Button";
            skillName.text = "Skill Name";
            skillCost.text = "Cost: ";
        }
    }

    private void Awake() {
        OnValidate();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterController>();
    }

    public void PressButton() {
        if (player.SkillPoints >= ability.SkillPointCost) {//if has the skill points to buy
            //add skill to player
            player.AddAbility(ability);
            player.SkillPoints = player.SkillPoints - ability.SkillPointCost;
            //remove the button
            gameObject.SetActive(false);
        }
    }
}
