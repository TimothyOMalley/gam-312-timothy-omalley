﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CreateAssetMenu(fileName = "New Ability", menuName = "ScriptableObjects/Ability", order = 2)]
public class AbilityDefinition : ScriptableObject{

    [SerializeField] string skillName;
    [SerializeField] enum Affects { self, environment }
    [SerializeField] Affects whatThisAbilityAffects = Affects.self; 

    [SerializeField] enum Type { heal, damage, translate, momentum }
    [SerializeField] Type whatTypeOfAbility = Type.heal;

    [SerializeField] float modifier = 0f;
    [SerializeField] enum Direction { forwards, backwards, up, down, left, right }
    [SerializeField] Direction direction = Direction.forwards;
     
    [SerializeField] enum Manifestation { touch } //alternative option could be projectile or ranged 
    [HideInInspector] //Right now there's only one variation so I'm just hiding this
    [SerializeField] Manifestation wayOfUse = Manifestation.touch;

    [SerializeField] int skillPointCost = 0;
    [SerializeField] GameObject VFX;
    [SerializeField] AudioSource SFX;

    public string SkillName { get { return skillName; } }
    public int SkillPointCost { get { return skillPointCost; } }

    public void UseAbility(GameObject player) {

        GameObject target = null;

        switch (wayOfUse) {
            case Manifestation.touch:
                break;
        }

        switch (whatThisAbilityAffects) {
            case Affects.self:
                target = player.GetComponent<CharacterController>().character.gameObject;
                break;
            case Affects.environment:
                try {
                    target = player.GetComponent<CharacterController>().LastCollision;
                } catch (MissingReferenceException) {
                    Debug.Log("No Collision To Activate On");
                }
                break;
        }

        switch (whatTypeOfAbility) {
            case Type.heal:
                target.GetComponent<Entity>().ChangeHealth(modifier);
                break;
            case Type.damage:
                target.GetComponent<Entity>().ChangeHealth(modifier*-1);
                break;
            case Type.translate:
                switch (direction) {
                    case Direction.forwards:
                        target.GetComponent<Rigidbody>().MovePosition(target.GetComponent<Rigidbody>().position + player.transform.forward * modifier);
                        break;
                    case Direction.backwards:
                        target.GetComponent<Rigidbody>().MovePosition(target.GetComponent<Rigidbody>().position + player.transform.forward * modifier * -1);
                        break;
                    case Direction.up:
                        target.GetComponent<Rigidbody>().MovePosition(target.GetComponent<Rigidbody>().position + player.transform.up * modifier);
                        break;
                    case Direction.down:
                        target.GetComponent<Rigidbody>().MovePosition(target.GetComponent<Rigidbody>().position + player.transform.up * modifier * -1);
                        break;
                    case Direction.right:
                        target.GetComponent<Rigidbody>().MovePosition(target.GetComponent<Rigidbody>().position + player.transform.right * modifier);
                        break;
                    case Direction.left:
                        target.GetComponent<Rigidbody>().MovePosition(target.GetComponent<Rigidbody>().position + player.transform.right * modifier * -1);
                        break;
                }
                break;
            case Type.momentum:
                switch (direction) {
                    case Direction.forwards:
                        target.GetComponent<Rigidbody>().AddForce((player.transform.forward*modifier), ForceMode.Impulse);
                        break;
                    case Direction.backwards:
                        target.GetComponent<Rigidbody>().AddForce((player.transform.forward * modifier * -1), ForceMode.Impulse);
                        break;
                    case Direction.up:
                        target.GetComponent<Rigidbody>().AddForce((player.transform.up * modifier), ForceMode.Impulse);
                        break;
                    case Direction.down:
                        target.GetComponent<Rigidbody>().AddForce((player.transform.up * modifier * -1), ForceMode.Impulse);
                        break;
                    case Direction.right:
                        target.GetComponent<Rigidbody>().AddForce((player.transform.right * modifier), ForceMode.Impulse);
                        break;
                    case Direction.left:
                        target.GetComponent<Rigidbody>().AddForce((player.transform.right * modifier * -1), ForceMode.Impulse);
                        break;
                }
                break;
        }
        try {
            Instantiate(VFX, target.transform.position, new Quaternion());
        }catch(Exception e) {
            Debug.Log("Tried to play VFX for ability " + skillName + " but ran into an error");
        }
    }
}
