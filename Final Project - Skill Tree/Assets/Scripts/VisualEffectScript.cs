﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualEffectScript : MonoBehaviour{
    [SerializeField] float duration = 0f;

    private void Update() {
        duration -= Time.deltaTime;
        if(duration <= 0) {
            Destroy(gameObject);
        }
    }
}
