﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPointUpgrade : MonoBehaviour{
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            other.GetComponent<CharacterController>().SkillPoints = other.GetComponent<CharacterController>().SkillPoints+1;
            Destroy(gameObject);
        }
    }
}
