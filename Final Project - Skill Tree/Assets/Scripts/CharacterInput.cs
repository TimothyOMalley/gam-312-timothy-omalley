﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInput : MonoBehaviour{
    [SerializeField] CharacterController character;
    public GameObject inventory,playerUI,skillTree;
    [SerializeField] InventoryController inventory_;

    public enum ControlScheme {player, inventory, skillTree}
    ControlScheme scheme = ControlScheme.player;

    private void Awake(){
        inventory.SetActive(true);
        playerUI.SetActive(true);
        skillTree.SetActive(true);
        inventory.SetActive(false);
        skillTree.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if(movementInput.magnitude >= 1.0f)
            movementInput = movementInput.normalized;

        switch (scheme) {
            case ControlScheme.player:
                character.InputM = movementInput;

                if (Input.GetButtonDown("Jump"))
                    character.run = true;
                else if (Input.GetButtonUp("Jump"))
                    character.run = false;

                if (Input.GetButtonDown("Inventory")) {
                    scheme = ControlScheme.inventory;
                    character.InputM = new Vector2(0, 0);
                    inventory.SetActive(true);
                    inventory.GetComponent<InventoryController>().UpdateStats();
                }

                if (Input.GetButtonDown("ToggleSkillTree")) {
                    scheme = ControlScheme.skillTree;
                    character.InputM = new Vector2(0, 0);
                    skillTree.SetActive(true);
                }

                if (Input.GetButtonDown("UseAbility")) {
                    character.UseAbility();
                }

                if (Input.GetButtonDown("SwapActiveAbility")) {
                    character.SwitchAbility();
                }

                break;

            case ControlScheme.inventory:
                inventory_.GiveInfo(movementInput);

                if(Input.GetButtonDown("Jump")) {
                    inventory_.Select();
                }

                if(Input.GetButtonDown("Inventory")) {
                    scheme = ControlScheme.player;
                    inventory.SetActive(false);
                }

                if(Input.GetButtonDown("Y")){
                    inventory_.RemoveSelected();
                }

                if (Input.GetButtonDown("L")){
                    character.PlayerStats.AlterStats(new StatSystem(), StatSystem.StatChangeType.Reset);
                }
                break;

            case ControlScheme.skillTree:
                if (Input.GetButtonDown("ToggleSkillTree")) {
                    scheme = ControlScheme.player;
                    skillTree.SetActive(false);
                }
                break;
        }
    }


}
