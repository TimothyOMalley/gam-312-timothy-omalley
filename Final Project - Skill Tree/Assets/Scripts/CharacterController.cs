﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour{
    [SerializeField] float walk = 3.0f;
    [SerializeField] float speedMod = 3.0f;
    [SerializeField] float regRotationSpeed = 100f;
    [SerializeField] float rotationSpeed = 100f;
    [SerializeField] float windUpFrames = 50f;
    [SerializeField] float percentilMaxHealthUpdate = 1f; //100% health recovered on health update tick 
    [SerializeField] float healthUpdateTick = 5f;
    [SerializeField] float currentTick = 0;
    [SerializeField] RawImage healthImage;
    [SerializeField] Text currentSkillPoints;
    [SerializeField] Text health, currentAbilityText;
    [SerializeField] AbilityDefinition currentAbility;
    List<AbilityDefinition> unlockedAbilities = new List<AbilityDefinition>();
    [SerializeField] int skillPoints = 0;
    int lastRecordedSkillPoints = 0;
    private float doubleTime, doubleTimeRotation;
    public bool run = false;

    StatSystem playerStats = new StatSystem();

    [SerializeField] Vector2 inputM; //input-Movement
    public Rigidbody character;
    [SerializeField] GameObject CubeMesh;
    [SerializeField] Animator spriteAnimator;
    [SerializeField] GameObject lastCollision;

    public GameObject LastCollision {
        get { return lastCollision; }
        set { lastCollision = value; }
    }


    public int SkillPoints {
        get { return skillPoints; }
        set { skillPoints = value; }
    }

    void Awake(){
        doubleTime = 2 * speedMod;
        doubleTimeRotation = 2 * rotationSpeed;
    }

    private void Update(){
        if(lastRecordedSkillPoints != skillPoints) {
            lastRecordedSkillPoints = skillPoints;
            currentSkillPoints.text = "Skill Points: " + skillPoints;
        }


        transform.Rotate(transform.up * inputM.x * rotationSpeed * Time.fixedDeltaTime);
        if (CubeMesh != null) {
            CubeMesh.transform.Rotate(transform.up * inputM.x * rotationSpeed * Time.fixedDeltaTime);
        }
        spriteAnimator.SetFloat("CubeColliderYRotation", gameObject.transform.rotation.eulerAngles.y);
        spriteAnimator.SetFloat("inputM", inputM.y);
        if (run){ //on run
            if(speedMod < doubleTime){ //increase the speed mod/rotation speed by a fraction of the double time
                speedMod += ((doubleTime - walk) / windUpFrames);
                rotationSpeed += ((doubleTimeRotation - regRotationSpeed) / windUpFrames);
            }
        }
        else{
            if(speedMod > walk) { //otherwiae DECREASE the speed mod/rotation speed by a fraction of double time
                speedMod -= ((doubleTime - walk) / windUpFrames);
                rotationSpeed -= ((doubleTimeRotation - regRotationSpeed) / windUpFrames);
            }
        }
        if(gameObject.GetComponent<Entity>().currentHealth < gameObject.GetComponent<Entity>().maxHealth){
            if(currentTick >= healthUpdateTick){
                gameObject.GetComponent<Entity>().currentHealth += gameObject.GetComponent<Entity>().maxHealth * percentilMaxHealthUpdate; //heal for 2.5% of max health 
                currentTick = 0f;
            }
            currentTick += Time.deltaTime;
        }
        if (gameObject.GetComponent<Entity>().currentHealth > gameObject.GetComponent<Entity>().maxHealth) { gameObject.GetComponent<Entity>().currentHealth = gameObject.GetComponent<Entity>().maxHealth; }
    }

    void FixedUpdate(){
        character.MovePosition(character.position + (transform.forward * inputM.y) * speedMod * Time.fixedDeltaTime * (1+(playerStats.GetStat(1).Number/10)));
        gameObject.GetComponent<Entity>().maxHealth = 100 * (1 + (playerStats.GetStat("Constitution").Number / 10));
        UpdateUI();
    }

    void UpdateUI(){
        health.text = "Health: " + gameObject.GetComponent<Entity>().currentHealth + "/" + gameObject.GetComponent<Entity>().maxHealth;
        healthImage.transform.localScale = new Vector3((gameObject.GetComponent<Entity>().currentHealth / gameObject.GetComponent<Entity>().maxHealth),1,1);
    }

    public Vector2 InputM
    {
        get { return inputM; }
        set { inputM = value; }
    }
    public StatSystem PlayerStats{ get { return playerStats; } } //PlayerStats ( get; )

    public void UseAbility() {
        if (currentAbility != null) {
            currentAbility.UseAbility(gameObject);
        }
    }

    public void AddAbility(AbilityDefinition abil) {
        foreach(AbilityDefinition x in unlockedAbilities) {
            if(x == abil) { unlockedAbilities.Remove(x); } //this allows for an "override" or "upgrade" of an already obtained ability
        }
        unlockedAbilities.Add(abil);
    }

    public void SwitchAbility() {
        if(unlockedAbilities.Count != 0) {
            bool foundTheAbility = false;
            bool abilitySet = false;
            AbilityDefinition tempAbil = null;
            foreach(AbilityDefinition abil in unlockedAbilities) {
                if (foundTheAbility) {
                    if (abilitySet == false) {
                        unlockedAbilities.Remove(tempAbil);
                        unlockedAbilities.Add(tempAbil);
                        currentAbility = abil;
                        currentAbilityText.text = "Current Ability: " + currentAbility.SkillName;
                        abilitySet = true;
                    }
                }
                if(currentAbility == abil) {
                    foundTheAbility = true;
                    tempAbil = abil;
                }
                else if(currentAbility == null) {
                    currentAbility = abil;
                    currentAbilityText.text = "Current Ability: " + currentAbility.SkillName;
                }
            }
        }
    }

}
