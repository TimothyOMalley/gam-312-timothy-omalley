﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBarTest : MonoBehaviour{
    private void OnTriggerEnter(Collider other){
        if (other.CompareTag("Player")){
            other.GetComponentInChildren<CharacterController>().PlayerStats.GetStat("Constitution").Number -= (other.GetComponentInChildren<CharacterController>().PlayerStats.GetStat("Constitution").Number + 10) / 4;
            other.GetComponentInParent<CharacterInput>().inventory.GetComponent<InventoryController>().UpdateStats();
        }
    }
}
